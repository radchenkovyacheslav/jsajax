var currentNewsId = 0;
var newsMaxId = 4;

$(function(){
	$("#prevBtn").click(function(){
		$("#preloader").show();
		currentNewsId--;
		if(currentNewsId < 0){
			currentNewsId = newsMaxId;
		}
		$("#message").load("ajax.php?newsid=" + currentNewsId,
			function(){
				$("#preloader").hide();
			});
	});
	$("#nextBtn").click(function(){
		$("#preloader").show();
		currentNewsId++;
		if(currentNewsId > newsMaxId){
			currentNewsId = 0;
		}
		$("#message").load("ajax.php?newsid=" + currentNewsId,
			function(){
				$("#preloader").hide();
			});
	});

	$("#message").load("ajax.php?newsid=" + currentNewsId);
});